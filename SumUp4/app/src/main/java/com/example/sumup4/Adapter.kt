package com.example.sumup4

import android.content.ClipData
import android.content.Context
import android.graphics.Color
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.NonNull
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.sumup4.databinding.ModelBinding
import com.google.android.material.snackbar.Snackbar
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.sqrt

class ItemAdapter(val context: Context, private val imgGallery: ArrayList<Item>): RecyclerView.Adapter<ItemAdapter.ImageViewHolder>()
  {

    lateinit var currentState: Array<Array<String>>
    var imgGaleriFilteredList = ArrayList<Item>()
      var clickCounter = 0;
      val arrPosition = mutableListOf<Button>()
      val arrPosition1 = mutableListOf<Int>()
      var rowSize:Int = 0
    init {
        imgGaleriFilteredList = imgGallery
        rowSize = sqrt(imgGaleriFilteredList.size.toDouble()).toInt();
        currentState = Array<Array<String>>(rowSize) { Array(rowSize) { " " } }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ImageViewHolder {
        return ImageViewHolder(ModelBinding.inflate(LayoutInflater.from(parent.context),parent, false))
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        val model = imgGaleriFilteredList[position]

        val string = context.resources.getString(R.string.winner)
        holder.image.setOnClickListener{ view ->
            gameOver(view)
            if(clickCounter % 2 == 0) {
                holder.image.setImageResource(model.imgSrc)
                arrPosition1.add(position)
                d("posit", "$position")

                currentState[position / rowSize][position % rowSize] = "X"

            } else {
                holder.image.setImageResource(model.imgSrc2)
                arrPosition1.add(position)
                currentState[position / rowSize][position % rowSize] = "O"
            }

            clickCounter++;

            var a = currentState[0][0]
            d("arrayX", "$a")

            //diagonali1
            for(i in currentState.indices) {
                if(currentState[i][i] == " " || currentState[i][i] != currentState[i + 1][i + 1]) {
                    break
                }

                if(i == currentState.size - 2) {
                    val win = currentState[i][i]
                    Snackbar.make(view, string.plus(win), Snackbar.LENGTH_SHORT).show()
                    view.findNavController().navigate(R.id.action_game_to_gameOver)
                    break;
                }
            }
            //diagonali2
             for (i in currentState.indices) {
                 if(currentState[i][rowSize - i - 1] == " " || currentState[i][rowSize - i - 1] != currentState[i + 1][rowSize - i - 2]) {
                     break
                 }
                 if(i == currentState.size - 2) {
                     val win = currentState[i][i]
                     Snackbar.make(view, string.plus(win), Snackbar.LENGTH_SHORT).show()
                    view.findNavController().navigate(R.id.action_game_to_gameOver)
                     break;
                 }
             }
            // shevamowmot row
            for (i in currentState.indices) {
                if(currentState[i][i] == " ") {
                    break;
                }

                for(j in currentState[i].indices) {
                    if(currentState[i][j] != currentState[i][j + 1]){
                        break
                    }

                    if(j == currentState.size - 2) {
                        val win = currentState[i][i]
                        Snackbar.make(view, string.plus(win), Snackbar.LENGTH_SHORT).show()
                        view.findNavController().navigate(R.id.action_game_to_gameOver)
                        break;
                    }
                }

            }
            // shevamowmot col
            for (i in currentState.indices) {
                if(currentState[i][i] == " ") {
                    break;
                }

                for(j in currentState[i].indices) {
                    if(currentState[j][i] != currentState[j + 1][i]){
                        break
                    }

                    if(j == currentState.size - 2) {
                        val win = currentState[i][i]
                        Snackbar.make(view, string.plus(win), Snackbar.LENGTH_SHORT).show()
                        view.findNavController().navigate(R.id.action_game_to_gameOver)
                        break;
                    }
                }

            }

            if (arrPosition1.contains(position)) {
                holder.image.isClickable = false
                holder.image.setBackgroundColor(Color.DKGRAY)
            }
        }

    }

    override fun getItemCount() = imgGaleriFilteredList.size

    inner class ImageViewHolder(private val binding: ModelBinding) : RecyclerView.ViewHolder(binding.root) {
        val image: ImageButton = binding.btnX
    }

      private fun gameOver(view:View) {
          if(arrPosition.size == imgGaleriFilteredList.size -1) {
              Snackbar.make(view, R.string.game_over, Snackbar.LENGTH_SHORT).show()
              view.findNavController().navigate(R.id.action_game_to_gameOver)
          }

      }

}