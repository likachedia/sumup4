package com.example.sumup4

import android.content.ClipData
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sumup4.databinding.FragmentGameBinding
import kotlin.math.floor
import kotlin.math.sqrt
import kotlin.random.Random

class Game : Fragment() {
    val args: GameArgs by navArgs()
    private var _binding: FragmentGameBinding? = null
    private val binding get() = _binding!!
    private lateinit var adapter: ItemAdapter
    private lateinit var items: ArrayList<Item>
    private lateinit var recyclerView: RecyclerView
    private  var btns: Int = 3
    private var span: Int = 3
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentGameBinding.inflate(inflater, container, false)
        btns = args.column

        span = btns * btns

        init()
        return binding.root
    }

    private fun init () {

        setData()
        binding.recyclerView.layoutManager = GridLayoutManager(requireContext(),btns)
        adapter = ItemAdapter(requireContext(), items)
        binding.recyclerView.adapter = adapter
        //    adapter.callback = { item, position ->
        //       editItem(position)
        //    }
    }

    private fun setData() {
            items = ArrayList<Item>()
            while (span > 0) {
                    items.add(Item(R.mipmap.close, R.mipmap.dryclean))
                span--
            }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}